import React, { createRef, useContext, useEffect, useState } from "react";
import imgshow from "../../Assets/Images/marketing mapa page- hero element.png";
import { ProjectContext } from "../../Context/MainContext";
import Card from "../Card/Card";
import CtaCard from "../CTACards/CtaCard";
import FiltersPC from "../Filters/Filters";
import FiltersMob from "../Filters/FiltersMob";
import Footer from "../Footer/Footer";
import Modal from "../Modal/Modal";
import Nav from "../Nav/Nav";
import Popup from "../Popup/Popup";
import "./MarketingMap.css";

const MarketingMap = () => {
  const {
    setHideButton,
    showHire,
    showApply,
    MainItemsShown,
    filterID,
    setShowMore,
    showMore,
    filterMode,
    allToggled,
    setHideLogo,
    inMap,
    setInMap,
  } = useContext(ProjectContext);

  useEffect(() => {
    setHideButton(false);
    setHideLogo(false);
    setInMap(true);
  }, []);

  return (
    <React.Fragment>
      <Nav />
      {showApply && <Modal showApply={showApply} show={showApply} />}
      {showHire && <Popup showHire={showHire} show={showHire} />}
      <div className="main-wrap">
        <img src={imgshow} alt="" className="picpic" />
        <div className="map-container">
          <div className="map-header">
            <h1>Macedonia Marketing Map</h1>
            <p>
              Провери кои маркетинг епскерти и агенции најмногу <br />
              одговарааат за твоите потреби. Бесплатно.
            </p>
          </div>
          <div className="map-content">
            {window.innerWidth < 770 ? <FiltersMob /> : <FiltersPC />}

            <div className={`map-cards`}>
              {!MainItemsShown.length && (
                <p>
                  Во овој момент нема достапни
                  {filterID === "agency" ? " компании" : " фриленсери"} од
                  избраните категории!
                </p>
              )}
              {MainItemsShown.map((el, i) => {
                return (
                  <Card
                    name={el.name}
                    type={el.TYPE}
                    desc={el.desc}
                    email={el.email}
                    link={el.link}
                    key={i}
                  />
                );
              })}
              {(filterMode || allToggled) && (
                <p
                  onClick={() => setShowMore(showMore + 6)}
                  className={
                    MainItemsShown.length < showMore ? "hideshow" : "showmore"
                  }
                >
                  Види повеќе
                </p>
              )}
            </div>
          </div>
        </div>
      </div>
      <CtaCard showHire={showHire} />

      <Footer />
    </React.Fragment>
  );
};

export default MarketingMap;
