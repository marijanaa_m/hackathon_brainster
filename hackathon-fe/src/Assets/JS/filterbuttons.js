const BTNS = [
  {
    title: "Marketing and Growth Strategies",
    clicked: false,
  },
  {
    title: "Social Search",
    clicked: false,
  },
  {
    title: "Social Ads",
    clicked: false,
  },
  {
    title: "Content and SEO",
    clicked: false,
  },
  {
    title: "Email Marketing",
    clicked: false,
  },
  {
    title: "Social Media",
    clicked: false,
  },
  {
    title: "Influencer Marketing",
    clicked: false,
  },
  {
    title: "Analytics",
    clicked: false,
  },
  {
    title: "Conversion Rate Optimization",
    clicked: false,
  },
  {
    title: "Web Design and Development",
    clicked: false,
  },
  {
    title: "Video Production",
    clicked: false,
  },
  {
    title: "Offline Marketing",
    clicked: false,
  },
  {
    title: "Amazon Management",
    clicked: false,
  },
  {
    title: "Brand Marketing",
    clicked: false,
  },
  {
    title: "PR",
    clicked: false,
  },
];

export default BTNS;
